FROM docker.io/library/ubuntu:22.04 AS base

ADD mattermost.cpp upload_img_w_inline.sh run.sh gen_plot.sh gnuplot.template /app/

RUN apt-get -y update \
    && apt-get -y --no-install-recommends install gnuplot sqlite3 libcurl4-openssl-dev build-essential g++ ca-certificates curl jq \
    && c++ -o /app/mattermost /app/mattermost.cpp -l curl \
    && dpkg --purge build-essential perl libperl5.28 perl-modules-5.28  liberror-perl libfile-fcntllock-perl libdpkg-perl libalgorithm-merge-perl libalgorithm-diff-xs-perl libalgorithm-diff-perl dpkg-dev \
    && chmod a+rx /app/*.sh \
    && apt-get -y autoremove --purge && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ENV PATH="/app:${PATH}"


###############################################################################
FROM base AS test

RUN  apt-get -y update \
     && apt-get -y --no-install-recommends install file \
     && gen_plot.sh 5 > /tmp/test.png \
     && file /tmp/test.png | grep -q "PNG image data" \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

CMD run.sh
