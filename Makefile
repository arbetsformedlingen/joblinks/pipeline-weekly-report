# This Makefile is provided for local development / run without Kubernetes

IMAGENAME = pipeline-weekly-report

ENV = test
#ENV = prod

# checkout from the secrets repo git@gitlab.com:arbetsformedlingen/joblinks/pipeline-weekly-report-secrets.git
SECRETSDIR = $(PWD)/../pipeline-weekly-report-secrets/$(ENV)

.ONESHELL:

all: build run


build:
	@if ! docker image inspect $(IMAGENAME) >/dev/null; then
		docker build -t $(IMAGENAME) . >&2
	fi


run: build
	@docker run -v $(PWD)/upload_img_w_inline.sh:/app/upload_img_w_inline.sh -v $(PWD)/run.sh:/app/run.sh -v $(SECRETSDIR):/secrets --rm -i $(IMAGENAME) bash -c ". /secrets/secrets.sh && /app/run.sh"


clean:
	if docker image inspect $(IMAGENAME) >/dev/null; then
		docker rmi $(IMAGENAME) >&2
	fi
