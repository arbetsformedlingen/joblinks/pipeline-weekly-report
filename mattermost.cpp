#include <iostream>
#include <curl/curl.h>
#include <curl/easy.h>
#include <string>

using namespace std; // bad practice


void uploadByMimeApi(string url, string channelId, string path, const char * mimetype, string token)
{
    url += "/api/v4/files";

    CURL *curl = curl_easy_init();
    curl_mime *mime;
    curl_mimepart *part;
    curl_mimepart *part2;
    curl_mimepart *part3;

    /* Build an HTTP form */
    mime = curl_mime_init(curl);
    part = curl_mime_addpart(mime);

    curl_mime_name(part, "files");
    curl_mime_filedata(part, path.c_str());
    curl_mime_type(part, mimetype);

    part2 = curl_mime_addpart(mime);
    curl_mime_data(part2, channelId.c_str(), CURL_ZERO_TERMINATED);
    curl_mime_name(part2, "channel_id");

    part3 = curl_mime_addpart(mime);
    curl_mime_data(part3, "test", CURL_ZERO_TERMINATED);
    curl_mime_name(part3, "client_ids");

    //header issue
    struct curl_slist *headers = NULL;
    static const char buf[] = "Expect:";
    string auth = "Authorization:Bearer ";
    auth += token;
    headers = curl_slist_append(headers, auth.c_str());
    headers = curl_slist_append(headers, buf);

    /* Post and send it. */
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_MIMEPOST, mime);
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fi);
    curl_easy_perform(curl);

    /* Clean-up. */
    curl_easy_cleanup(curl);
    curl_mime_free(mime);
}


int main(int argc, char *argv[]) {

  if(argc != 6) {
    cerr << "**** please supply args: server-url chanid file mimetype authtoken\n";
    exit(1);
  }

  uploadByMimeApi(argv[1],
                  argv[2],
                  argv[3],
                  argv[4],
                  argv[5]);
  }
