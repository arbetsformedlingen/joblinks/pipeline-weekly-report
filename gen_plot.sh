#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


report_span_in_days=${1:-5}


# Download time series and db.sqlite for pipeline-stats
cd /tmp
curl -L -O 'https://gitlab.com/api/v4/projects/30191577/repository/archive.tar.gz'
tar -zxf archive.tar.gz
cd activity-data-collector-data-main-*/pipeline-stats


### Determine list of partners
version=$(curl https://gitlab.com/arbetsformedlingen/joblinks/pipeline-gitops-for-servers/-/raw/master/bin/run_prod.sh | grep ^PIPELINECOMMIT | sed -e 's|PIPELINECOMMIT=||' -e 's|[[:space:]]*||g')
partners=$({ curl https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/"$version"/conf/common_settings.sh ; echo 'echo $include_import,$exclude_import' ; } | bash - | tr ',' ' ')


DATE="$(date +%Y-%m-%d)"

cp "$self_dir"/gnuplot.template plot.gp
echo -n "plot " >> plot.gp

### Create header files and table files
for P in $partners; do
    ID=$(sqlite3 db.sqlite 'select scraper_id from scraper WHERE name = "'"$P"'"')

    tail -n "$report_span_in_days" "$ID".csv > "$ID".data
    echo -n '"'$ID'.data" using 1:2 with linespoints title "'$P'",' >> plot.gp
done

gnuplot plot.gp
