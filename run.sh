#!/usr/bin/env bash
set -eEu -o pipefail

test_or_prod="${1:-test}"

report_span_in_days=5

DATE="$(date +%V)"
PLOTFILE=/tmp/scraperreport_week_"$DATE".png


gen_plot.sh "$report_span_in_days" > "$PLOTFILE"


bash upload_img_w_inline.sh "Weekly Scraper Report Week $DATE" "$PLOTFILE"
